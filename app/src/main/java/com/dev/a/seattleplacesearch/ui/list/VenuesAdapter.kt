package com.dev.a.seattleplacesearch.ui.list

import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.dev.a.seattleplacesearch.R
import com.dev.a.seattleplacesearch.api.model.Geocode
import com.dev.a.seattleplacesearch.api.model.Response
import com.dev.a.seattleplacesearch.api.model.Venue
import com.dev.a.seattleplacesearch.databinding.RowVenueBinding
import com.dev.a.seattleplacesearch.util.calculateDistance
import com.squareup.picasso.Picasso

class VenuesAdapter(val venueSearchResponse: Response) : RecyclerView.Adapter<VenuesAdapter.ViewHolder>() {

    companion object {
        val TAG: String = VenuesAdapter::class.java.simpleName
    }

    var callback: Callback? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<RowVenueBinding>(
            LayoutInflater.from(parent.context),
            R.layout.row_venue,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return venueSearchResponse.venues.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Log.i(TAG, "Position: $position")
        val venue = venueSearchResponse.venues[position]
        holder.bind(venue, venueSearchResponse.geocode)
        holder.binding.root.setOnClickListener { callback?.onItemClicked(venue) }
    }


    class ViewHolder(var binding: RowVenueBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(venue: Venue, geocode: Geocode) {

            var url: String
            venue.categories[0].icon.apply {
                url = getCategoryIconUrl(prefix, suffix, "100")
            }
            Picasso.get().load(url).into(binding.ivIcon)

            binding.tvName.text = venue.name
            binding.tvCategory.text = TextUtils.join(", ", venue.categories.map { it.name })

            val distanceFromCityCenter = calculateDistance(
                venue.location.lat,
                venue.location.lng,
                geocode.feature.geometry.center.lat,
                geocode.feature.geometry.center.lng
            )

            binding.tvDistance.text = binding.root.context.getString(
                R.string.distance_from_city_center,
                "%.2f".format(distanceFromCityCenter)
            )
        }
    }

    interface Callback {
        fun onItemClicked(venue: Venue)
    }
}