package com.dev.a.seattleplacesearch.ui.detail

import com.dev.a.seattleplacesearch.api.model.Geocode
import com.dev.a.seattleplacesearch.api.model.Venue

data class MapMarker(val color: String, val label: String, val lat: Double, val lng: Double)

private fun buildMapUrl(mapMarkers: List<MapMarker>): String {
    var url = "https://maps.googleapis.com/maps/api/staticmap"
    url += "?zoom=14"
    url += "&size=720x600"
    url += "&maptype=roadmap"
    url += "&key=AIzaSyBM0lrqiTUICCE072LsfiRv9YsanJN-CI0"
    mapMarkers.forEach {
        url += "&markers=color:${it.color}|label:${it.label}|${it.lat},${it.lng}"
    }
    return url
}

fun buildMapUrl(venue: Venue, geocode: Geocode): String {
    val markers = mutableListOf<MapMarker>().apply {
        add(MapMarker("blue", "City Center", geocode.feature.geometry.center.lat, geocode.feature.geometry.center.lng))
        add(MapMarker("red", venue.name, venue.location.lat, venue.location.lng))
    }
    return buildMapUrl(markers)
}