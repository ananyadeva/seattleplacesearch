package com.dev.a.seattleplacesearch.ui.detail

import android.os.Bundle
import android.text.TextUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.dev.a.seattleplacesearch.R
import com.dev.a.seattleplacesearch.api.model.Geocode
import com.dev.a.seattleplacesearch.api.model.Venue
import com.dev.a.seattleplacesearch.databinding.ActivityDetailBinding
import com.dev.a.seattleplacesearch.util.calculateDistance
import com.squareup.picasso.Picasso

class DetailActivity : AppCompatActivity() {

    companion object {
        var TAG: String = DetailActivity::class.java.simpleName
        const val KEY_VENUE = "KEY_VENUE"
        const val KEY_GEOCODE = "KEY_GEOCODE"
    }

    lateinit var binding: ActivityDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        val venue = intent.getSerializableExtra(KEY_VENUE) as Venue
        val geocode = intent.getSerializableExtra(KEY_GEOCODE) as Geocode

        setupMap(venue, geocode)
        setupVenueDetails(venue, geocode)
    }

    private fun setupMap(venue: Venue, geocode: Geocode) {

        val url = buildMapUrl(venue, geocode)
        Picasso.get().load(url).into(binding.ivImage)
    }

    private fun setupVenueDetails(venue: Venue, geocode: Geocode) {
        supportActionBar?.title = venue.name
        binding.tvCategory.text = TextUtils.join(", ", venue.categories.map { it.name })
        binding.tvAddress.text = TextUtils.join("\n", venue.location.formattedAddress)

        val distanceFromCityCenter = calculateDistance(
            venue.location.lat,
            venue.location.lng,
            geocode.feature.geometry.center.lat,
            geocode.feature.geometry.center.lng
        )

        binding.tvDistance.text = getString(R.string.distance_from_city_center, "%.2f".format(distanceFromCityCenter))
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}