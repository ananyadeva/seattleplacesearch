package com.dev.a.seattleplacesearch.ui.search

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.dev.a.seattleplacesearch.R
import com.dev.a.seattleplacesearch.api.model.FoursquareVenueSearchResponse
import com.dev.a.seattleplacesearch.api.framework.Resource
import com.dev.a.seattleplacesearch.databinding.ActivitySearchBinding
import com.dev.a.seattleplacesearch.ui.list.ListActivity

class SearchActivity : AppCompatActivity() {

    companion object {
        val TAG: String = SearchActivity::class.java.simpleName
    }

    private lateinit var binding: ActivitySearchBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search)
        setSupportActionBar(binding.toolbar)

        val viewModel = ViewModelProviders.of(this).get(SearchActivityViewModel::class.java)
        viewModel.venueSearchLiveData.observe(this, venuesObserver)
        binding.tilSearch.editText!!.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                viewModel.searchVenues(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

    }

    private val venuesObserver = Observer<Resource<FoursquareVenueSearchResponse>> { resource ->
        Log.i(TAG, resource.toString())
        when (resource) {
            is Resource.Loading -> handleLoadingState()
            is Resource.Failure -> handleFailure(resource)
            is Resource.Success -> handleSuccess(resource)
        }

    }

    private fun handleSuccess(resource: Resource.Success<FoursquareVenueSearchResponse>) {
        val venues = resource.data.response.venues
        binding.tvSearchStatus.text =
            resources.getQuantityString(
                R.plurals.activity_search_tv_search_status_success,
                venues.size,
                venues.size
            )

        binding.btnViewAll.isEnabled = resource.data.response.venues.isNotEmpty()
        binding.btnViewAll.setOnClickListener {
            val intent = Intent(this, ListActivity::class.java)
            intent.putExtra(ListActivity.KEY_SEARCH_RESULT, resource.data.response)
            startActivity(intent)
        }
    }

    private fun handleFailure(resource: Resource.Failure<FoursquareVenueSearchResponse>) {
        if (resource.throwable is SearchActivityViewModel.SearchException) {
            binding.tvSearchStatus.text = ""
        } else {
            binding.tvSearchStatus.text =
                getString(R.string.activity_search_tv_search_status_error, resource.throwable.message)
        }
        binding.btnViewAll.isEnabled = false
    }

    private fun handleLoadingState() {
        binding.tvSearchStatus.text = getString(R.string.activity_search_tv_search_status_loading)
        binding.btnViewAll.isEnabled = false
    }


}
