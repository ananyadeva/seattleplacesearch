package com.dev.a.seattleplacesearch.util

fun calculateDistance(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
    val radiusOfEarth = 6371.0

    val dLat = deg2rad(lat2) - deg2rad(lat1)
    val dLon = deg2rad(lon2) - deg2rad(lon1)

    val a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.sin(dLon / 2) * Math.sin(
            dLon / 2
        )

    val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
    val d = radiusOfEarth * c // Distance in km
    return d
}

private fun deg2rad(deg: Double): Double {
    return deg * Math.PI / 180.0
}
