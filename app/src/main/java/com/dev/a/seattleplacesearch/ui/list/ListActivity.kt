package com.dev.a.seattleplacesearch.ui.list

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.dev.a.seattleplacesearch.R
import com.dev.a.seattleplacesearch.api.model.Geocode
import com.dev.a.seattleplacesearch.api.model.Response
import com.dev.a.seattleplacesearch.api.model.Venue
import com.dev.a.seattleplacesearch.databinding.ActivityListBinding
import com.dev.a.seattleplacesearch.ui.detail.DetailActivity

class ListActivity : AppCompatActivity() {

    companion object {
        val TAG: String = ListActivity::class.java.simpleName
        const val KEY_SEARCH_RESULT = "KEY_SEARCH_RESULT"
    }

    private lateinit var binding: ActivityListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_list)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        val searchResponse = intent.getSerializableExtra(KEY_SEARCH_RESULT) as Response
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        val venuesAdapter = VenuesAdapter(searchResponse)
        venuesAdapter.callback = object : VenuesAdapter.Callback {
            override fun onItemClicked(venue: Venue) {
                Log.i(TAG, "onItemClicked")
                launchDetail(venue, searchResponse.geocode)
            }

        }
        binding.recyclerView.adapter = venuesAdapter
    }

    fun launchDetail(venue: Venue, geocode: Geocode) {
        val intent = Intent(this@ListActivity, DetailActivity::class.java)
        intent.putExtra(DetailActivity.KEY_VENUE, venue)
        intent.putExtra(DetailActivity.KEY_GEOCODE, geocode)
        startActivity(intent)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}