package com.dev.a.seattleplacesearch.api.repository

import com.dev.a.seattleplacesearch.api.framework.Resource
import com.dev.a.seattleplacesearch.api.framework.RetrofitFactory
import com.dev.a.seattleplacesearch.api.model.FoursquareVenueSearchError
import com.dev.a.seattleplacesearch.api.model.FoursquareVenueSearchResponse
import com.dev.a.seattleplacesearch.api.service.FoursquareVenuesService

class VenuesApiRepository {

    companion object {
        val TAG = VenuesApiRepository::class.java.simpleName
        const val BASE_URL = "https://api.foursquare.com"
        const val DEFAULT_NEAR_STRING = "Seattle, +WA"
        const val DEFAULT_RESULTS_LIMIT = 50
    }

    suspend fun searchVenues(
        searchString: String,
        near: String = DEFAULT_NEAR_STRING,
        limit: Int = DEFAULT_RESULTS_LIMIT
    ): Resource<FoursquareVenueSearchResponse> {
        val retrofit = RetrofitFactory.getRetrofitInstance(BASE_URL)
        val foursquarePlacesService = retrofit.create(FoursquareVenuesService::class.java)

        return try {
            val response = foursquarePlacesService.getFoursquareVenueSearchResponse(searchString, near, limit)
            if (response.isSuccessful) {
                Resource.Success(response.body()!!)
            } else {

                // Handling of API failures

                val converter = retrofit.responseBodyConverter<FoursquareVenueSearchError>(
                    FoursquareVenueSearchError::class.java,
                    emptyArray()
                )

                //Using Retrofit's type converter to convert error response
                val error =
                    converter.convert(response.errorBody()!!) //TODO this is a little unsafe. Handling should be added for null errorBody.

                val message = error?.meta?.errorDetail ?: "API call failure"
                Resource.Failure(Exception(message))
            }
        } catch (e: Exception) {
            Resource.Failure(Exception("Something went wrong"))
        }
    }

}