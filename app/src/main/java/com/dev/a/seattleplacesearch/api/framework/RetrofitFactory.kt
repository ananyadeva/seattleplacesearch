package com.dev.a.seattleplacesearch.api.framework

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitFactory {

    private val baseUrlToRetrofitInstance = mutableMapOf<String, Retrofit>()

    fun getRetrofitInstance(baseUrl: String): Retrofit {
        return baseUrlToRetrofitInstance.getOrPut(baseUrl, {
            return Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        })
    }


}