package com.dev.a.seattleplacesearch.ui.search

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dev.a.seattleplacesearch.api.model.FoursquareVenueSearchResponse
import com.dev.a.seattleplacesearch.api.framework.Resource
import com.dev.a.seattleplacesearch.api.repository.VenuesApiRepository
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.lang.Exception

class SearchActivityViewModel : ViewModel() {

    companion object {
        const val DEBOUNCE_INTERVAL_MILLIS = 700L
        const val MIN_SEARCH_CHARS = 2
    }

    val venueSearchLiveData = MutableLiveData<Resource<FoursquareVenueSearchResponse>>()

    var searchVenuesJob: Job? = null

    fun searchVenues(searchString: String) {
        searchVenuesJob?.cancel() // Cancel the last launched coroutine (which is now of no use) as the user is typing again

        if (searchString.trim().length < MIN_SEARCH_CHARS) {
            venueSearchLiveData.postValue(
                Resource.Failure(
                    SearchException(
                        "Search string should be longer than $MIN_SEARCH_CHARS characters"
                    )
                )
            )
            return
        }

        venueSearchLiveData.postValue(Resource.Loading())
        searchVenuesJob = viewModelScope.launch {

            delay(DEBOUNCE_INTERVAL_MILLIS) // Wait before executing the API call as the user might be typing

            val placesApiRepository = VenuesApiRepository()

            // For the sake of simplicity, we're using the API data models directly.
            // Ideally, UI should expose its own data models and API data models should be mapped into them.
            // Also, instead of accessing the API repository directly, a manager class should be used which would wrap API access and business login (if any).
            val resource = placesApiRepository.searchVenues(searchString)
            venueSearchLiveData.postValue(resource)
        }
    }

    class SearchException(message: String) : Exception(message)

}