package com.dev.a.seattleplacesearch.api.service

import com.dev.a.seattleplacesearch.api.model.FoursquareVenueSearchResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

private const val clientId = "GQVZLG5M1PWS3D5EB3SFFQRTJBJADZGHVXP2UE0S5ADEMZVK"
private const val clientSecret = "SK4DHLNPCPKHKYDXL2ZLR1YDNW3O2CGMFW5YOWIZUIYRDIYX"
private const val apiVersion = "20190601"

interface FoursquareVenuesService {

    @GET("/v2/venues/search?client_id=$clientId&client_secret=$clientSecret&v=$apiVersion")
    suspend fun getFoursquareVenueSearchResponse(
        @Query("query") searchString: String,
        @Query("near") near: String,
        @Query("limit") limit: Int
    ): Response<FoursquareVenueSearchResponse>
}