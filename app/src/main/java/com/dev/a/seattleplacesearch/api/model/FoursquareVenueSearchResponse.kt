package com.dev.a.seattleplacesearch.api.model

import java.io.Serializable

// Serializables are used for the sake of simplicity

data class FoursquareVenueSearchResponse(val response: Response) : Serializable

data class Response(val venues: List<Venue>, val geocode: Geocode) : Serializable

data class Venue(val id: String, val name: String, val location: Location, val categories: List<Category>) :
    Serializable {

    data class Location(val lat: Double, val lng: Double, val formattedAddress: List<String>) : Serializable

    data class Category(val id: String, val name: String, val icon: Icon) : Serializable {
        data class Icon(val prefix: String, val suffix: String) : Serializable
    }
}

data class Geocode(val feature: Feature) : Serializable {
    data class Feature(val name: String, val geometry: Geometry) : Serializable {
        data class Geometry(val center: Center) : Serializable {
            data class Center(val lat: Double, val lng: Double) : Serializable
        }
    }
}

data class FoursquareVenueSearchError(val meta: Meta) : Serializable {
    data class Meta(val code: Int, val errorDetail: String) : Serializable
}

